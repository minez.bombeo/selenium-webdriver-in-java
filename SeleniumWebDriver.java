import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;

public class SeleniumWebDriver {
    public static void main(String[] args) {
        //Selenium WebDriver
        //Create driver object for Chrome
        //
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Kasutaja\\Desktop\\SeleniumWebDriver\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get("https://www.mindvalley.com");
        //System.out.println(driver.getTitle());
        //System.out.println(driver.getCurrentUrl());
        //System.out.println(driver.getPageSource());

        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"js-navi\"]/div/a")).click();

        new WebDriverWait(driver,20,50).until(ExpectedConditions.textToBePresentInElementLocated(By.linkText("Don't remember your password?"), "Don't remember your password?"));
        driver.findElement(By.xpath("//*[@id=\"1-email\"]")).sendKeys("test@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"widget-container\"]/div/div/form/div/div/div/div[2]/div[2]/span/div/div/div/div/div/div/div/div/div[2]/div[3]/div[2]/div[1]/div/input")).sendKeys("");

        // driver.quit();

        //WebElement element = driver.findElement(By.xpath("//*[@id=\"1-email\"]"));
    }
}
