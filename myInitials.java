public class myInitials {
    //String in1 = "12345";
    String in2 = "M   M";
    String in3 = "MM MM";
    String in4 = "M M M";
    String mi1 = "V   V";
    String mi2 = " V V ";
    String mi3 = "  V  ";
    String ln1 = "BBBB ";
    String ln2 = "B   B";
    String nln1 = "H   H";
    String nln2 = "HHHHH";

    public static void main(String[] args) {
        myInitials myIn = new myInitials();
        //System.out.println(myIn.in1);
        System.out.println(myIn.in2 + "  " + myIn.mi1 + "  " + myIn.ln1 + "  " + myIn.nln1);
        System.out.println(myIn.in2 + "  " + myIn.mi1 + "  " + myIn.ln2 + "  " + myIn.nln1);
        System.out.println(myIn.in3 + "  " + myIn.mi1 + "  " + myIn.ln2 + "  " + myIn.nln1);
        System.out.println(myIn.in4 + "  " + myIn.mi1 + "  " + myIn.ln1 + "  " + myIn.nln2);
        System.out.println(myIn.in2 + "  " + myIn.mi1 + "  " + myIn.ln2 + "  " + myIn.nln1);
        System.out.println(myIn.in2 + "  " + myIn.mi2 + "  " + myIn.ln2 + "  " + myIn.nln1);
        System.out.println(myIn.in2 + "  " + myIn.mi3 + "  " + myIn.ln1 + "  " + myIn.nln1);
    }
}
