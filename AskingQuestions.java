import java.util.Scanner; //import a scanner class to read keyboard input

public class AskingQuestions {
    public static void main(String[] args) {
        Scanner ask = new Scanner(System.in); //construct a scanner object
        String work, country;
        int years;

        System.out.print("Where do you work? "); //******prompt message
        work = ask.nextLine(); //read in next string and store in work

        System.out.print("In what country? "); //******prompt message
        country = ask.next(); //read in next string and store in country

        System.out.print("How long have you been working in " + work + "?"); //******prompt message
        years = ask.nextInt(); //read in next number and store in years

    }
}
