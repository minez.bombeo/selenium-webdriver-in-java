public class UsingVariables {
    public static void main(String[] args) {
        int room = 113;
        double size = 2.71828;
        String cs = "Computer Science";

        System.out.println("This is room " + room);
        System.out.println("e is close to " + size);
        System.out.println("I am learning a bit about " + cs);
    }
}
